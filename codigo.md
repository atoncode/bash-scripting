# Códigos 

¿Deberíamos agregar estas cuatro líneas de código a cada script de bash?

```shell
set -o errexit
set -o nounset
set -o pipefail
set -o xtrace
```

Bueno, probablemente, pero con algunas advertencias. Consideremos algunos escenarios.

## Argumentos opcionales
Es posible que desee ejecutar su script con cero o más argumentos. Imagina que queremos mejorar el 8-all-the-things.sh script para que cuando lo ejecutemos sin argumentos, por defecto usará el Dockerfile-build archivo. Todavía podemos usar set -o nounset, pero para garantizar que no falle el script cuando no se proporciona ningún argumento, debemos establecer un valor predeterminado para el parámetro posicional faltante. Podemos hacer esto usando la sintaxis Bash para valores de variables predeterminados:

docker_file = "$ { 1: -Dockerfile-build }"
Agregando :- después de que el nombre de la variable le dice a Bash que sigue la cadena :- debe usarse como el valor cuando la variable no se ha establecido.

Datos confidenciales
Si su script va a funcionar con datos confidenciales, contraseñas o tokens secretos, por lo general, se considera una mala práctica imprimir estos valores en la consola o en archivos de registro que podrían persistir para una visualización futura. Si su script va a procesar datos confidenciales o secretos, puede agregar set +o xtrace para desactivar el seguimiento del comando en la línea antes de un comando que contendrá un valor sensible o secreto. Una vez que haya terminado con el procesamiento sensible o secreto, puede volver a activar el rastreo con set -o xtrace. Asegúrese de probar su script con algunos datos o secretos simulados que pueda cambiar o invalidar después para asegurarse de que no los imprima.

Soporte de depuración
Otro escenario en el que es posible que no desee utilizar el rastreo es cuando va a distribuir sus scripts a los usuarios finales y no desea distraerlos con los detalles internos de cómo el guión funciona. En ese escenario, es posible que desee usar un DEBUG variable de entorno y una declaración condicional para permitir el rastreo cuando sea necesario. En condiciones normales, sus usuarios pueden ejecutar los scripts con el rastreo apagado. Cuando haya un problema, podrá solicitar a sus usuarios finales que habiliten el rastreo con export DEBUG=TRUE y agregue los registros resultantes a un ticket de soporte.

```shell
if [ [ "$ { DEBUG: -FALSE }" != "FALSO" ] ]; entonces
  set -o xtrace
fi
```
