#!/bin/bash
# Read using while

while true; do
  read -p "Escriba su nombre: " answer
  
  if [ "$answer" != "" ]; then
    : "${1#"${1%%[![:space:]]*}"}"
    : "${_%"${_##*[![:space:]]}"}"
    
    echo $answer
    break
  fi
done
