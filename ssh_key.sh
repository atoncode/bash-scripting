#!/bin/bash

user=fredych
## SSH-key lenght
ssh_key_lenght='2048'

# Create '.ssh' directory in user's home directory
if ! [ -d /home//.ssh ] ;then
  sudo mkdir /home//.ssh
fi

# Generate ssh-key pair and move them to correspondig user's '.ssh/' dir.
ssh_file_name="./_ssh"
(
echo ""
echo ""
echo ""
) | ssh-keygen -t rsa -b 

sudo mv -i "" ".pub" /home//.ssh/
sudo chown : /home/

cat /home//.ssh/.pub
